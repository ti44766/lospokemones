<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/saludo', function () {
    return 'Hola, que hace!';
});
//Ruta de recursos GET POST PUT DELETE
Route::resource('pokemones',
App\Http\Controllers\PokemonController::class);


//Ruta Normal
//Route::get('/pokemones',
//[App\Http\Controllers\PokemonController::class,'index']);