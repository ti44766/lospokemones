@extends('plantilla')
@section('titulo') 
- Listado
@endsection
@section('principal')
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Habilidades</th>
                                <th>Pokedex</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pokemones as $i =>$row )
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$row->nombre}}</td>
                                    <td>{{$row->tipo}}</td>
                                    <td>{{$row->habilidades}}</td>
                                    <td>{{$row->pokedex}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
  @endsection